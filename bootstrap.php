<?php

// include the composer autoloader for autoloading packages
require_once __DIR__ . '/vendor/autoload.php';

/**
 * [env Obtiene variables de entorno]
 * @param  string $var Llave de la variable de entorno
 * @return string      Retorna data de la variable de entorno
 */
function env(string $var): string{
	$env = parse_ini_file(__DIR__ . '/.env');
	return $env[$var];
}

function getEntityManager(): \Doctrine\ORM\EntityManager{
	$dbParams = [
		'dbname' => env('DATABASE_NAME'),
		'user' => env('DATABASE_USER'),
		'password' => env('DATABASE_PSWD'),
		'host' => env('DATABASE_HOST'),
		'driver' => env('DATABASE_DRIVER'),
		'port' => env('DATABASE_PORT'),
		'charset' => env('DATABASE_CHARSET'),
	];

	$config = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(
		[env('ENTITY_DIR')],
		env('DEBUG'),
		ini_get('sys_temp_dir'),
		null,
		false
	);

	$config->setAutoGenerateProxyClasses(true);
	return \Doctrine\ORM\EntityManager::create($dbParams, $config);
}

function autoload_classes($class_name) {
	$filename = PROJECTPATH . '/' . str_replace('\\', '/', $class_name) . '.php';
	if (is_file($filename)) {
		include_once $filename;
	}
}
