<?php
session_start();

//directorio del proyecto
define("PROJECTPATH", $_SERVER['DOCUMENT_ROOT']);

require_once __DIR__ . '/bootstrap.php';

//registramos el autoload autoload_classes
spl_autoload_register('autoload_classes');

try {
	$app = new \Core\App;
	$app->render();
} catch (\Exception $e) {
	print_r($e->getMessage());
}
