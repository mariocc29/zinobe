<?php
namespace Src\Controllers;

use Src\Models\Users;
use Src\Traits\CustomerData;

/**
 * Gestiona el controlador de usuarios
 */
class RegisterController extends \Core\Controller {

	use CustomerData;

	public $countries;

	function __construct() {
		$this->countries = CustomerData::getCountries();
	}

	public function index() {
		$this->render('register', ['countries' => $this->countries]);
	}

	public function store($request) {
		$user = getEntityManager()->getRepository(Users::class)->findBy(['email' => $request['email']]);
		if (count($user) > 0) {
			$this->render('register', ['error' => 'La cuenta ya existe', 'countries' => $this->countries]);
		} else {
			$em = getEntityManager();

			$user = new Users;
			$user->setFullname($request['fullname']);
			$user->setDni($request['dni']);
			$user->setEmail($request['email']);
			$user->setCountry($request['country']);
			$user->setPassword($request['password']);

			try {
				$em->persist($user);
				$em->flush();

				$_SESSION['logged'] = true;
				header('Location: /user', true, 303);
				die();
			} catch (\Exception $e) {
				$this->render('register', ['error' => $e->getMessage(), 'countries' => $this->countries]);
			}
		}
	}
}
