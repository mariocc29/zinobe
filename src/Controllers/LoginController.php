<?php
namespace Src\Controllers;

use Src\Models\Users;

/**
 * Gestiona el controlador de usuarios
 */
class LoginController extends \Core\Controller {

	public function index() {
		$this->render('login');
	}

	public function auth(array $request) {
		$user = getEntityManager()->getRepository(Users::class)->findBy(['email' => $request['email'], 'password' => $request['password']]);
		if (count($user) === 0) {
			$this->render('login', ['error' => 'La cuenta no existe o la contraseña no es válida']);
		} else {
			$_SESSION['logged'] = true;
			header('Location: /user', true, 303);
			die();
		}
	}

	public function logout() {
		unset($_SESSION['logged']);
		header('Location: /login', true, 303);
		die();
	}
}
