<?php
namespace Src\Controllers;

use Src\Models\Users;

/**
 * Gestiona el controlador de usuarios
 */
class UserController extends \Core\Controller {

	/**
	 * [index Importa los registros del modelo]
	 * @return void [Exporta los usuarios del modelo]
	 */
	public function index() {
		if ($_SESSION['logged']) {
			$users = getEntityManager()->getRepository(Users::class)->findAll();
			$this->render('users', ['users' => $users]);
		} else {
			header('Location: /login', true, 303);
			die();
		}
	}

	/**
	 * [search Realiza la búsqueda de los usuarios]
	 * @param  string $criteria Texto de búsqueda
	 * @return void           [Exporta los usuarios filtrados del modelo]
	 */
	public function search(string $criteria) {
		if ($_SESSION['logged']) {
			$users = getEntityManager()->createQueryBuilder()
				->select(['u'])
				->from(Users::class, 'u')
				->where("u.fullname LIKE '%{$criteria}%'")
				->orWhere("u.email LIKE '%{$criteria}%'")
				->getQuery()
				->getResult();

			$this->render('users', ['users' => $users]);
		} else {
			header('Location: /login', true, 303);
			die();
		}
	}
}
