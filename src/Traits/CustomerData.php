<?php
namespace Src\Traits;

use GuzzleHttp\Client;

trait CustomerData {

	public static function getCountries(): array{
		$service = new Client(['base_uri' => 'https://restcountries.eu/rest/v2/all']);
		$response = $service->get('', []);
		return json_decode($response->getBody()->getContents(), true);
	}

}
