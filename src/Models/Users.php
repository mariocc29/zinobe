<?php
namespace Src\Models;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table(name="users")
 * @ORM\Entity
 */
class Users {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="fullname", type="string", length=255, nullable=false)
	 */
	private $fullname;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="dni", type="string", length=255, nullable=false)
	 */
	private $dni;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=255, nullable=false)
	 */
	private $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="country", type="string", length=255, nullable=false)
	 */
	private $country;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="password", type="string", length=255, nullable=false)
	 */
	private $password;

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getFullname() {
		return $this->fullname;
	}

	/**
	 * @param string $fullname
	 *
	 * @return self
	 */
	public function setFullname($fullname) {
		$this->fullname = $fullname;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getDni() {
		return $this->dni;
	}

	/**
	 * @param string $dni
	 *
	 * @return self
	 */
	public function setDni($dni) {
		$this->dni = $dni;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param string $email
	 *
	 * @return self
	 */
	public function setEmail($email) {
		$this->email = $email;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * @param string $country
	 *
	 * @return self
	 */
	public function setCountry($country) {
		$this->country = $country;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * @param string $password
	 *
	 * @return self
	 */
	public function setPassword($password) {
		$this->password = $password;

		return $this;
	}
}
