<?php
namespace Core;

use Core\View;

/**
 *
 */
class Controller {

	protected function render(string $template, array $parameters = []) {
		View::render($template, $parameters);
	}
}
