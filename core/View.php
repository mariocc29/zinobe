<?php
namespace Core;

class View {

	const VIEWS_PATH = 'src/Views/';

	public static function render(string $template, array $parameters = []) {

		$filename = self::VIEWS_PATH . $template . '.phtml';
		if (!file_exists($filename)) {
			throw new \Exception("Error Processing View ${template}");
		}

		ob_start();
		extract($parameters);
		include $filename;
		$str = ob_get_contents();
		ob_end_clean();
		echo $str;
	}

}
