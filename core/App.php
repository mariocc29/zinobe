<?php
namespace Core;

/**
 * Clase que renderiza el controlador solicitado
 */
class App {

	private $_controller = null;

	private $_method = 'index';

	private $_params = [];

	const CONTROLLERS_PATH = PROJECTPATH . "/src/Controllers/";

	const NAMESPACE_CONTROLLERS = "\Src\Controllers\\";

	function __construct() {
		$url = $this->parseUrl();
		if (count($url) > 0) {
			if (!file_exists(self::CONTROLLERS_PATH . ucfirst($url[0]) . 'Controller.php')) {
				throw new \Exception("Error Processing Controller");

			}

			$this->_controller = self::NAMESPACE_CONTROLLERS . ucfirst($url[0]) . 'Controller';
			$this->_method = isset($url[1]) ? $url[1] : 'index';
			$this->_params = isset($url[2]) ? array_slice($url, 2) : [];
		} else {
			$this->_controller = self::NAMESPACE_CONTROLLERS . ucfirst(env('WELCOME_CONTROLLER')) . 'Controller';
		}
	}

	private function parseUrl(): array{
		if (strlen(trim($_SERVER['REQUEST_URI'], "/")) > 0) {
			return explode("/", filter_var(trim($_SERVER['REQUEST_URI'], "/"), FILTER_SANITIZE_URL));
		}
		return [];
	}

	public function render() {
		$controller = new $this->_controller;
		if (!method_exists($controller, $this->_method)) {
			throw new \Exception("Error Processing Method", 1);
		}

		call_user_func_array([$controller, $this->_method], [array_merge($this->_params, $_POST)]);
	}
}
